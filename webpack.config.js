/** plugins */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ContextReplacementPlugin = require('webpack').ContextReplacementPlugin;
const MinifyPlugin = require("babel-minify-webpack-plugin");

const path = require('path');

/** define configuration */
const config = (dev) => {
    return {
        entry: [
            "./src/js/main.js",
            "./src/scss/bundle.scss"
        ],
        output: {
            filename: dev ? 'bundle.js' : 'bundle.min.js',
            path: path.resolve(__dirname, dev ? 'dist' : 'prod')
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: dev ? 'styles.css' : 'styles.min.css',
            }),
            new ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|cz)$/),
            new MinifyPlugin({ }, {
                test: dev ? null : /\.\/prod\/\.js$/
            })
        ],
        devtool: dev ? 'eval-source-map' : 'none',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: [
                                    '@babel/preset-env'
                                ]
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpg|gif|eot|woff|woff2|ttf)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'assets',
                                publicPath: './assets'
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader
                        },
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: dev
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                implementation: require('node-sass'),
                                sassOptions: {
                                    sourceMap: dev,
                                    sourceComments: dev,
                                    sourceMapEmbed: dev,
                                    outFile: dev ? {
                                        filename: 'styles.css',
                                          path: path.resolve(__dirname, 'dist')
                                    } : null,
                                    outputStyle: dev ? 'expanded' : 'compressed'
                                }
                            }
                        }
                    ]
                },
            ],
        }
    }
};

module.exports = (env, argv) => {
    return config(argv.mode === 'development');
};