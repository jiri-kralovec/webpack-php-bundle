$.fn.datify = function (options) {
    let defaults = {
        'fewSecondsAgo': 'few seconds ago',
        'aMinuteAgo': 'a minute ago',
        'minutesAgo': '#number minutes ago',
        'anHourAgo': 'an hour ago',
        'todayAt': 'today at #time',
        'yesterdayAt': 'yesterday at #time'
    };
    let opts = $.extend({}, defaults, options);
    let currentTime = new Date();
    $(this).each(function () {
        var datifyObject = new Datify($(this), opts, currentTime);
        datifyObject.reformat();
    });

};

class Datify {
    constructor(jqueryObject, options, currentTime) {
        this.jqueryObject = jqueryObject;
        this.currentTime = currentTime;
        this.options = options;
    }

    reformat() {
        var notificationTime = new Date(this.jqueryObject.attr('datetime'));
        var relativeTime = this.getRelativeTime(notificationTime);
        this.jqueryObject.html(relativeTime);
    }

    getRelativeTime(absolutTime,) {
        var timeDeltaSeconds = Math.round((this.currentTime.getTime() - absolutTime.getTime()) / 1000);
        var yesterday = new Date();
        yesterday.setDate(this.currentTime.getDate() - 1);

        if (timeDeltaSeconds <= 30) {
            return this.options['fewSecondsAgo'];
        } else if (timeDeltaSeconds < 90) {
            return this.options['aMinuteAgo'];
        } else if (timeDeltaSeconds < 2700 /* 45 minutes */) {
            var minutes = Math.round(timeDeltaSeconds / 60);
            return this.options['minutesAgo'].replace('#number', minutes);
        } else if (timeDeltaSeconds < 4500 /* 1 hour 15 seconds */) {
            return this.options['anHourAgo'];
        } else if (this.currentTime.getYear() === absolutTime.getYear() && this.currentTime.getMonth() === absolutTime.getMonth() && this.currentTime.getDate() === absolutTime.getDate()) {
            var time = absolutTime.getHours() + ':' + this.formatMinutes(absolutTime.getMinutes());
            return this.options['todayAt'].replace('#time', time);
        } else if (yesterday.getYear() === absolutTime.getYear() && yesterday.getMonth() === absolutTime.getMonth() && yesterday.getDate() === absolutTime.getDate()) {
            var time = absolutTime.getHours() + ':' + this.formatMinutes(absolutTime.getMinutes());
            return this.options['yesterdayAt'].replace('#time', time);
        }
        return absolutTime.toLocaleString();
    }

    formatMinutes(minutes) {
        if (minutes < 10) {
            minutes = '0' + minutes;
        }
        return minutes;
    }
}