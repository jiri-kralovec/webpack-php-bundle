/** jQuery */
import { $, jQuery } from 'jquery';

/** Import libraries from node_modules */
import 'jquery-ui-sortable/jquery-ui.min';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';
import "@fortawesome/fontawesome-free/webfonts/fa-brands-400.woff2";
import "@fortawesome/fontawesome-free/webfonts/fa-regular-400.woff2";
import "@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2";

import 'admin-lte/plugins/bootstrap/js/bootstrap.bundle.min';
import 'admin-lte/plugins/select2/js/select2.full.min';
import 'admin-lte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min';
import 'admin-lte/plugins/moment/moment.min';
import 'admin-lte/plugins/inputmask/min/jquery.inputmask.bundle.min';
import 'admin-lte/plugins/bootstrap-switch/js/bootstrap-switch.min';
import 'admin-lte/plugins/sweetalert2/sweetalert2.min';
import 'admin-lte/plugins/toastr/toastr.min';
import 'admin-lte/dist/js/adminlte';
import 'admin-lte/plugins/summernote/summernote-bs4.min';

import 'bootstrap-fileinput/js/fileinput';
import 'bootstrap-fileinput/themes/fas/theme';
import 'bootstrap-fileinput/themes/explorer-fa/theme';
import 'bootstrap-fileinput/js/locales/cs';
import 'bootstrap-fileinput/img/loading-sm.gif';
import 'bootstrap-fileinput/img/loading.gif';
import 'bootstrap-daterangepicker/daterangepicker';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker';

import 'nette-forms/src/assets/netteForms.min';
import 'nette.ajax.js/nette.ajax';
import 'ublaboo-datagrid/assets/datagrid';
import 'ublaboo-datagrid/assets/datagrid-instant-url-refresh';
import 'ublaboo-datagrid/assets/datagrid-spinners';
/** Import local libraries */
import './vendor/jquery.datify';
import './vendor/jquery.letterpic';

