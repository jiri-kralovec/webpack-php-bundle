/** import third-party libraries */
import './vendor';

/** run */
$(function () {
    $.nette.init();

    $(".letterpic").letterpic();


    //Add text editor
    $('.compose-textarea:not([readonly])').summernote({
        height: 150
    });
    $('input[required],select[required],textarea[required]').each(function () {
        $(this).addClass('is-warning');
    });

    initDatepickers();
    initSwitchCheckbox();
    initSelects();

});

function initSelects() {
    //Initialize Select2 Elements
    $(".select2:not(.initialised):not([readonly])").each(function () {
        $(this).select2();
        $(this).addClass('initialised');
    });

    $(".select2bs4:not(.initialised):not([readonly])").each(function () {
        $(this).select2({
            theme: 'bootstrap4'
        });
        $(this).addClass('initialised');
    });
}

function initSwitchCheckbox() {
    $("input[data-bootstrap-switch]:not(.initialised)").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
        $(this).addClass('initialised');
    });
}

function initDatepickers() {
    $('input[data-type=datetime]:not(.initialised):not([readonly])').each(function () {
        var dateRangeOptions = {
            singleDatePicker: $(this).data('singledatepicker'),
            timePicker24Hour: $(this).data('timepicker24hour'),
            timePicker: $(this).data('timepicker'),
            showDropdowns: $(this).data('showdropdowns'),
            autoApply: $(this).data('autoapply'),
            locale: {
                format: $(this).data('format')
            }
        };
        if ($(this).val()) {
            dateRangeOptions.startDate = $(this).val();
        }
        $(this).daterangepicker(dateRangeOptions);
        $(this).addClass('initialised');
    });
}

$.nette.ext('baseReinit', {
    complete: function () {
        initDatepickers();
        initSwitchCheckbox();
        initSelects();
    }
});
$(document).on('ajaxSuccess', function () {

});